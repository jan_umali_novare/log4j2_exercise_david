package hk.com.novare.loggingexercise.user.service;

import hk.com.novare.loggingexercise.user.entity.UserEntity;
import hk.com.novare.loggingexercise.user.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public ResponseEntity<UserEntity> createUser(UserEntity userEntity) {
        try{
            if(userEntity.getUsername() != null || userEntity.getPassword() != null){
                LOGGER.debug("Inserting the user with the username: {}", userEntity.getUsername());
                return new ResponseEntity<>(userRepository.save(userEntity), HttpStatus.OK);
            } else {
                throw new Exception();
            }

        } catch (Exception e){
            LOGGER.fatal("The request body should not have null values. The values are: Username - {} and Password - {}"
            , userEntity.getUsername(), userEntity.getPassword());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @Override
    public List<UserEntity> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public ResponseEntity<UserEntity> getUser(int id) {
        try{
            if(userRepository.existsById(id)){
                LOGGER.debug("User with the id: {} has been retrieved.", id);
                return new ResponseEntity<>(userRepository.findById(id).get(), HttpStatus.OK);
            } else {
                LOGGER.debug("An entity with the id: {} does not exist.", id);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (IllegalArgumentException iae){
            LOGGER.error(iae.getMessage(), iae);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            LOGGER.fatal(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @Override
    public ResponseEntity<UserEntity> updateUser(UserEntity userEntity) {
        try{
            if(userRepository.existsById(userEntity.getId())){
                LOGGER.debug("User with the id: {} has been verified.", userEntity.getId());
                return new ResponseEntity<>(userRepository.save(userEntity), HttpStatus.OK);
            } else {
                LOGGER.debug("An entity with the id: {} does not exist.", userEntity.getId());
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (IllegalArgumentException iae){
            LOGGER.error(iae.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            LOGGER.fatal(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @Override
    public void deleteUser(int id) {
        LOGGER.warn("Deleting returns void if the id does not exist, can add validation but did not for demonstration purposes.");
        userRepository.deleteById(id);
    }
}
