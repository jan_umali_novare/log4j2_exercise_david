package hk.com.novare.loggingexercise.user.service;

import hk.com.novare.loggingexercise.user.entity.UserEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    ResponseEntity<UserEntity> createUser(UserEntity userEntity);
    List<UserEntity> getAllUsers();
    ResponseEntity<UserEntity> getUser(int id);
    ResponseEntity<UserEntity> updateUser(UserEntity userEntity);
    void deleteUser(int id);
}
