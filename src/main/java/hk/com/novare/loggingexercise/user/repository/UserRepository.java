package hk.com.novare.loggingexercise.user.repository;

import hk.com.novare.loggingexercise.user.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer>{
}
