package hk.com.novare.loggingexercise.user.controller;

import hk.com.novare.loggingexercise.user.entity.UserEntity;
import hk.com.novare.loggingexercise.user.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);
    private UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping("/users")
    public ResponseEntity<UserEntity> createUser(@RequestBody UserEntity userEntity){
        LOGGER.info("Creating a user with the username: {}", userEntity.getUsername());
        return userService.createUser(userEntity);
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserEntity>> getAllUsers(){
        LOGGER.info("Retrieving a list of users from the database");
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @PutMapping("/users")
    public ResponseEntity<UserEntity> updateUser(@RequestBody UserEntity userEntity){
        LOGGER.info("Updating a specific user from the database with the id: {}", userEntity.getId());
        return userService.updateUser(userEntity);
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<UserEntity> getUser(@PathVariable(name = "userId") int id){
        LOGGER.info("Retrieving a user from the database with the id: {}", id);
        return userService.getUser(id);
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity deleteUser(@PathVariable(name = "userId") int id){
        LOGGER.info("Deleting a specific user from the database with the id: {}", id);
        userService.deleteUser(id);
        return new ResponseEntity(HttpStatus.OK);
    }


    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity noRequestBody(){
        LOGGER.error("No Request Body");
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
